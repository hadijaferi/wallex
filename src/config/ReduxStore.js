import {createStore, applyMiddleware, combineReducers} from 'redux';

import ReduxThunk from 'redux-thunk';

/**
 * reducers
 */

import articleReducers from '../redux/reducers/articleReducers';

const rootReducer = combineReducers({
  articleReducers: articleReducers,
});

export const ReduxStore = createStore(rootReducer, applyMiddleware(ReduxThunk));
