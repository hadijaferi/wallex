import 'react-native-gesture-handler';
import React from 'react';
import {Platform, ActivityIndicator, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider} from 'react-redux';
import {StatusBar} from 'react-native';
import {ReduxStore} from './ReduxStore';
import Screens from '../constants/screens';

/**
 *
 *
 * Screens
 *
 */

import Home from '../screens/home';
import Article from '../screens/article';

if (Platform.OS === 'ios') {
  const enableScreens = require('react-native-screens').enableScreens;
  enableScreens();
}
const Stack = createStackNavigator();

const options = {
  headerShown: false,
  // transitionSpec: {
  // open: animationConfig,
  // close: animationConfig,
  // },
};

// Gets the current screen from navigation state
const getActiveRouteName = (state) => {
  const route = state.routes[state.index];

  if (route.state) {
    // Dive into nested navigators
    return getActiveRouteName(route.state);
  }

  return route.name;
};

const navigationRef = React.createRef();
function App() {
  return (
    <Provider store={ReduxStore}>
      <NavigationContainer ref={navigationRef}>
        <StatusBar hidden />
        <Stack.Navigator initialRouteName={Screens.Home}>
          <Stack.Screen
            name={Screens.Home}
            component={Home}
            options={options}
          />
          <Stack.Screen
            name={Screens.Article}
            component={Article}
            options={options}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export function navigate(name, params = {}) {
  navigationRef.current?.navigate(name, params);
}
export default App;
