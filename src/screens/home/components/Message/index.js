import React, {Component} from 'react';
import {AppRegistry, View, Text} from 'react-native';

export default class Message extends Component {
  state = {
    ws: null,
    text: '',
  };

  // send = (data) => this.state.ws.send(data);
  componentDidMount() {
    this._handleWebSocketSetup();
  }

  _handleWebSocketSetup = () => {
    const ws = new WebSocket('wss://echo.websocket.org');
    ws.onopen = () => {
      console.log('Open!');
      setInterval(() => {
        ws.send('Hello wallex');
      }, 1000);
    };
    ws.onmessage = (e) => {
      console.log(e, 'onmessage');
      this.setState({text: e.data});
    };
    ws.onerror = (error) => {
      console.log(error, 'error');
    };
    // ws.onclose = (e) =>
    //   this.reconnect ? this._handleWebSocketSetup() : console.log(e, 'onclose');
    // this.setState({ws});
  };

  render() {
    return (
      <View
        style={{
          // margin: 10,
          flex: 2,
          backgroundColor: '#a6a6a6',
          justifyContent: 'center',
          alignItems: 'center',
          width: '90%',
          height: '90%',
        }}>
        <Text>{this.state.text}</Text>
      </View>
    );
  }
}

/*

return (
    <View
      style={{
        // margin: 10,
        flex: 2,
        backgroundColor: '#a6a6a6',
        justifyContent: 'center',
        alignItems: 'center',
        width: '90%',
        height: '90%',
      }}>
      <Text>{text}</Text>
    </View>
  );
*/
