import React, {useState, useEffect} from 'react';
import screens from '../../../../constants/screens';
import {navigate} from '../../../../config/AppNavigatior';
import {
  View,
  Text,
  FlatList,
  Platform,
  TouchableOpacity,
  Image,
} from 'react-native';
export function Articles(props) {
  useEffect(() => {});
  const renderCardItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => navigate(screens.Article, item)}
        style={{
          flexDirection: 'row',
          backgroundColor: '#d9d9d9',
          minHeight: 100,
          margin: 5,
        }}>
        <View style={{flex: 1}}>
          <Image
            style={{width: 100, height: 100}}
            source={{
              uri: item.image,
            }}
          />
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>{item.title}</Text>
          <Text>{item.author}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={{
        flex: 8,
        backgroundColor: '#d9d9d9',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
      }}>
      <View style={{width: '90%', height: '90%', backgroundColor: '#a6a6a6'}}>
        <FlatList
          renderItem={renderCardItem}
          data={props.article}
          // data={[{id:1},{id:1},{id:1},{id:1}]}
          contentContainerStyle={{
            paddingVertical: Platform.select({
              ios: 20,
              android: 20,
            }),
          }}
        />
      </View>
    </View>
  );
}
