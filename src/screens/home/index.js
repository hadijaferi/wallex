import * as React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  BackHandler,
  Platform,
  Image,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {connect} from 'react-redux';
import {articleActions} from '../../redux/actions/articleActions';
import {SAVE_ARTICLES} from '../../redux/types/actionTypes';
import Message from './components/Message';
import {Articles} from './components/Articles';
class Screen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.articleActions({type: SAVE_ARTICLES, payload: {}});
  }

  render() {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#d9d9d9',
        }}>
        <Message />
        <Articles article={this.props.Article} />
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  const {
    articleReducers: {Article},
  } = state;
  return {Article: Article};
}

export default connect(mapStateToProps, {articleActions})(Screen);
