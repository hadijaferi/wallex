import * as React from 'react';
import {View, Text, Image} from 'react-native';
import {connect} from 'react-redux';

class Screen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    let data = this.props.route.params;
    return (
      <View style={{flex: 1, backgroundColor: '#d9d9d9', alignItems: 'center'}}>
        <View
          style={{
            flex: 2,
            marginTop: 20,
            justifyContent: 'flex-start',
            alignItems: 'center',
            width: '100%',
          }}>
          <Image
            resizeMode={'cover'}
            style={{width: '90%', height: 250}}
            source={{
              uri: data.image,
            }}
          />
        </View>
        <View
          style={{
            flex: 3,
            width: '90%',
            marginTop: 20,
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
          }}>
          <Text>{data.title}</Text>
          <Text>{data.author}</Text>
          <Text>{data.category}</Text>
          <Text>{data.content}</Text>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {};
}

export default connect(mapStateToProps, {})(Screen);
