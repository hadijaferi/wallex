/**
 *
 * Name of screens to use in navigation
 *
 */
const Screens = {
  Article: 'Article',
  Home: 'Home',
};
export default Screens;
