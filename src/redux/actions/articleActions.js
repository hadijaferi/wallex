import {SAVE_ARTICLES} from '../types/actionTypes';
import {dispatchItemToRedux} from './commonActions';
import {ARTICLES} from '../types/reducerTypes';
import db from '../../db.json';
export function articleActions({type, payload}) {
  return function (dispatch, getState) {
    switch (type) {
      case SAVE_ARTICLES: {
        return new Promise(function (resolve, reject) {
          // dispatch(
          //   dispatchItemToRedux({
          //     type: ARTICLES,
          //     // payload: filteredArr.length > 1,
          //     payload: [1, 2],
          //   }),
          // );
          let articles = db.articles;
          let categories = db.categories;
          let articles_categoreis = db.articles_categoreis;
          let users = db.users;
          const filteredArr = articles.reduce((acc, current) => {
            const x = acc.find((item) => item.author === current.author);
            if (!x) {
              return acc.concat([current]);
            } else {
              return acc;
            }
          }, []);
          let filteredArrDup = filteredArr;
          let cate = [];
          filteredArr.map((ele, ind) => {
            let key = ele.author;
            let x = categories.filter((ele, ind) => ele.id === key);
            let y = x.filter((ele, ind) => {
              return ele.name;
            });
            if (!ele.category) {
              ele.category = (y[0].name && [y[0].name]) || '';
            }
            if (ele.author) {
              let m = users.filter((ele, ind) => {
                return ele.id === key;
              });
              let z = m.filter((ele, ind) => {
                return ele.name;
              });
              ele.author = (m[0] && m[0].name) || '';
            }
          });

          dispatch(
            dispatchItemToRedux({
              type: ARTICLES,
              payload: filteredArr,
            }),
          );
        });

        break;
      }

      default: {
        return new Promise.reject();
        break;
      }
    }
  };
}
