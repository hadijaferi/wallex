import {ARTICLES} from '../types/reducerTypes';

const INITIAL_STATE = {
  Article: {},
};

function articleReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ARTICLES: {
      return {...state, Article: action.payload};
    }
    default:
      return state;
  }
}
export default articleReducer;
