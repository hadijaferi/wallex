/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/config/AppNavigatior';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
